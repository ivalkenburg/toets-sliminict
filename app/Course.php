<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function students()
    {
        return $this->hasManyThrough(Student::class, Group::class);
    }

    public function getNameAttribute($value)
    {
        return unserialize($value);
    }

    public function setNameAttribute($value)
    {
        return $this->attributes['name'] = serialize($value);
    }

    public function getDescriptionAttribute($value)
    {
        return unserialize($value);
    }

    public function setDescriptionAttribute($value)
    {
        return $this->attributes['description'] = serialize($value);
    }

    public function asLocale($key)
    {
        return $this->$key[config('app.locale')] ?? $this->$key[config('app.fallback_locale')];
    }

    public function price($full = true, $mutator = null)
    {
        if ($full) {
            $mutator = $mutator ?: config('app.course_price_mutator');

            return str_replace(
                ',00', '', number_format(round($this->price * $mutator, 1), 2, ',', '.')
            );
        } else {
            return str_replace(
                ',00', '', number_format($this->price, 2, ',', '.')
            );
        }
    }

    public static function visible()
    {
        return static::whereVisible(true);
    }
}
