<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'starts_at',
        'ends_at',
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class)->withTimestamps()->orderBy('name');
    }

    public function addStudent(Student $student)
    {
        return $this->students()->attach($student);
    }

    public function removeStudent(Student $student)
    {
        return $this->students()->detach($student);
    }

    public function syncStudents($students)
    {
        return $this->students()->sync($students);
    }
}
