<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCourse;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function store(StoreCourse $request)
    {
        Course::create(
            $request->only('name', 'price', 'description', 'visible', 'lessons')
        );

        return redirect(route('courses.index'))->with('flash', __('Course saved'));
    }

    public function create()
    {
        return view('admin.courses.create', [
            'locales'  => config('app.locales'),
            'fallback' => config('app.fallback_locale'),
        ]);
    }

    public function index(Request $request)
    {
        $courses = Course::withCount('groups')->orderBy('name')->get();

        if ($request->ajax() || $request->wantsJson()) {
            return $courses;
        }

        return view('admin.courses.index', compact('courses'));
    }

    public function edit(Course $course)
    {
        return view('admin.courses.edit', [
            'course'   => $course,
            'locales'  => config('app.locales'),
            'fallback' => config('app.fallback_locale'),
        ]);
    }

    public function update(Course $course, StoreCourse $request)
    {
        $course->update($request->toArray());

        return redirect(route('courses.index'))->with('flash', __('Course updated'));
    }

    public function destroy(Course $course, Request $request)
    {
        $course->delete();

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'status'  => 'success',
                'message' => __('Course deleted'),
            ]);
        }

        return redirect(route('courses.index'))->with('flash', __('Course deleted'));
    }
}
