<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGroup;
use App\Student;
use Illuminate\Http\Request;

class GroupsController extends Controller
{
    public function index(Request $request)
    {
        $groups = Group::with('course')->withCount('students');

        if ($request->has('course')) {
            $groups->where('course_id', $request->get('course'));
        }

        $groups = $groups->get();

        if ($request->ajax() || $request->wantsJson()) {
            return $groups;
        }

        return view('admin.groups.index', compact('groups'));
    }

    public function show($group)
    {
        $group = Group::with(['students', 'course'])->findOrFail($group);

        return view('admin.groups.show', compact('group'));
    }

    public function create()
    {
        return view('admin.groups.create', [
            'courses'  => Course::all(['id', 'name'])->sortBy('name.'.config('app.locale')),
            'students' => Student::orderBy('name')->get(['id', 'name']),
        ]);
    }

    public function store(StoreGroup $request)
    {
        $group = Group::create($request->except('students'));

        $group->syncStudents($request->students);

        return redirect(route('groups.show', $group->id))->with('flash', __('Group saved'));
    }

    public function update(Group $group, StoreGroup $request)
    {
        $group->update($request->except('students'));

        $group->syncStudents($request->students);

        return redirect(route('groups.show', $group->id))->with('flash', __('Group updated'));
    }

    public function destroy(Group $group, Request $request)
    {
        $group->delete();

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'status'  => 'success',
                'message' => __('Group deleted'),
            ]);
        }

        return redirect(route('groups.index'))->with('flash', __('Group deleted'));
    }

    public function edit($group)
    {
        $group = Group::with('students')->findOrFail($group);

        return view('admin.groups.edit', [
            'group'    => $group,
            'courses'  => Course::all(['id', 'name'])->sortBy('name.'.config('app.locale')),
            'students' => Student::orderBy('name')->get(['id', 'name']),
        ]);
    }
}
