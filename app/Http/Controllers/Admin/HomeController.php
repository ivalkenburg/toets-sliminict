<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Meta;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('admin.index', [
            'fallback' => config('app.fallback_locale'),
            'locales'  => config('app.locales'),
            'body'     => Meta::find('index_body'),
        ]);
    }

    public function store(Request $request)
    {
        Meta::set('index_body', $request->body);

        if ($request->ajax() || $request->wantsJson()) {
            return [
                'status'  => 'success',
                'message' => __('Index body updated'),
            ];
        }

        return back()->with('flash', __('Index body updated'));
    }
}
