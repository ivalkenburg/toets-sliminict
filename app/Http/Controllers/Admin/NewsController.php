<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewsArticle;
use App\NewsArticle;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $articles = NewsArticle::latest()->get();

        if ($request->ajax() || $request->wantsJson()) {
            return $articles;
        }

        return view('admin.news.index', compact('articles'));
    }

    public function store(StoreNewsArticle $request)
    {
        $article = NewsArticle::create([
            'user_id' => auth()->id(),
            'title'   => $request->title,
            'body'    => $request->body,
        ]);

        return redirect(route('news.show', $article->id))->with('flash', __('News article saved'));
    }

    public function edit(NewsArticle $article)
    {
        return view('admin.news.edit', [
            'article'  => $article,
            'locales'  => config('app.locales'),
            'fallback' => config('app.fallback_locale'),
        ]);
    }

    public function create()
    {
        return view('admin.news.create', [
            'locales'  => config('app.locales'),
            'fallback' => config('app.fallback_locale'),
        ]);
    }

    public function update(NewsArticle $article, StoreNewsArticle $request)
    {
        $article->update($request->toArray());

        return redirect(route('news.show', $article->id))->with('flash', __('News article updated'));
    }

    public function destroy(NewsArticle $article, Request $request)
    {
        $article->delete();

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'status'  => 'success',
                'message' => __('News article deleted'),
            ]);
        }

        return redirect(route('news.index'))->with('flash', __('News article deleted'));
    }
}
