<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudent;
use App\Http\Requests\StoreStudentComment;
use App\Mail\StudentMail;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StudentsController extends Controller
{
    public function index()
    {
        $students = Student::orderBy('name')->get();

        if (request()->expectsJson()) {
            return $students;
        }

        return view('admin.students.index', compact('students'));
    }

    public function destroy(Student $student)
    {
        $student->delete();

        if (request()->expectsJson()) {
            return response()->json([
                'status'  => 'success',
                'message' => __('Student deleted'),
            ]);
        }

        return redirect(route('students.index'))->with('flash', __('Student deleted'));
    }

    public function create()
    {
        return view('admin.students.create', [
            'groups' => Group::all('id', 'name'),
        ]);
    }

    public function edit(Student $student)
    {
        $groups = Group::all('id', 'name');

        return view('admin.students.edit', compact('student', 'groups'));
    }

    public function store(StoreStudent $request)
    {
        $student = new Student();

        if ($request->hasFile('picture')) {
            $student->picture = Storage::disk('public')->put('students', $request->file('picture'));
        }

        $this->fillStudent($student, $request);

        return redirect(route('students.show', $student->id))->with('flash', __('Student saved'));
    }

    public function update(Student $student, StoreStudent $request)
    {
        if ($request->hasFile('picture')) {
            $student->changePicture(Storage::disk('public')->put('students', $request->file('picture')));
        } elseif ($request->no_picture) {
            $student->changePicture();
        }

        $this->fillStudent($student, $request);

        return redirect(route('students.show', $student->id))->with('flash', __('Student updated'));
    }

    public function show($student)
    {
        $student = Student::with(['comments', 'groups'])->findOrFail($student);

        if (request()->expectsJson()) {
            return $student;
        }

        return view('admin.students.show', compact('student'));
    }

    public function showEmailForm(Student $student)
    {
        return view('admin.students.email', compact('student'));
    }

    public function emailStudent(Student $student, Request $request)
    {
        $this->validate($request, [
            'subject' => 'required|max:80',
            'body'    => 'required|min:5',
        ]);

        $student->sendMail(
            new StudentMail($request->subject, $request->body, $student, auth()->user())
        );

        return redirect(route('students.show', $student->id))->with('flash', __('Email send'));
    }

    public function postComment(Student $student, StoreStudentComment $request)
    {
        $student->addComment($request);

        return redirect(route('students.show', $student->id))->with('flash', __('Comment saved'));
    }

    public function leaveGroup(Student $student, Group $group)
    {
        $student->leaveGroup($group);

        if (request()->expectsJson()) {
            return [
                'status'  => 'success',
                'message' => __('Student removed from group'),
            ];
        }
    }

    protected function fillStudent(Student $student, $request)
    {
        $student->fill($request->only('name', 'residence', 'email', 'birthday'))->save();

        $student->syncGroups($request->groups);

        return $student;
    }
}
