<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ActivateUser;
use App\Http\Requests\StoreUser;
use App\Mail\UserActivation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::orderBy('name', 'asc')->get();

        return view('admin.users.index', compact('users'));
    }

    public function destroy(User $user)
    {
        $user->delete();

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json([
                'status'  => 'success',
                'message' => __('User deleted'),
            ]);
        }

        return redirect('users.index')->with('flash', __('User deleted'));
    }

    public function store(StoreUser $request)
    {
        $this->addUser($request);

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json(['status' => 'success']);
        }

        return redirect(route('users.index'));
    }

    protected function addUser(Request $request)
    {
        $user = User::add($request->only('name', 'email'));

        Mail::to($user)->send(new UserActivation($user));
    }

    public function showActivateForm($token)
    {
        $user = User::where('activation_token', $token)->firstOrFail();

        return view('admin.users.activate', compact('user'));
    }

    public function activateUser(ActivateUser $request)
    {
        $user = User::where([
            'activation_token' => $request->token,
            'email'            => $request->email,
        ])->first();

        if (is_null($user)) {
            return back()->withErrors(['email' => trans('passwords.email_token_mismatch')]);
        }

        $user->update([
            'password'         => bcrypt($request->password),
            'activation_token' => null,
        ]);

        auth()->login($user, true);

        return redirect(route('admin.home'));
    }
}
