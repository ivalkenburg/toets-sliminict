<?php

namespace App\Http\Controllers;

use App\Course;

class CoursesController extends Controller
{
    public function index()
    {
        $courses = Course::visible()->orderBy('name')->get();

        return view('courses.index', compact('courses'));
    }
}
