<?php

namespace App\Http\Controllers;

use App\Meta;
use App\NewsArticle;

class HomeController extends Controller
{
    public function index()
    {
        $body = Meta::find('index_body');
        $news = NewsArticle::last();

        return view('index', compact('body', 'news'));
    }
}
