<?php

namespace App\Http\Controllers;

use App\NewsArticle;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        $articles = NewsArticle::latest()->paginate(5);

        if (request()->ajax() || request()->wantsJson()) {
            return $articles;
        }

        return view('news.index', compact('articles'));
    }

    public function show(NewsArticle $article)
    {
        return view('news.show', compact('article'));
    }
}
