<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\App;

class SetLocale
{
    public function handle($request, Closure $next)
    {
        if ($this->hasLocale($request->get('lang'))) {
            $this->setLocale($request->get('lang'));

            return $next($request)->cookie(cookie()->forever('lang', $request->get('lang')));
        } elseif ($this->hasLocale($request->cookie('lang'))) {
            $this->setLocale($request->cookie('lang'));

            return $next($request);
        } else {
            $this->setLocale(config('app.locale'));
        }

        return $next($request);
    }

    protected function setLocale($lang)
    {
        Carbon::setLocale($lang);
        App::setLocale($lang);
    }

    protected function hasLocale($lang)
    {
        return in_array($lang, config('app.locales'));
    }
}
