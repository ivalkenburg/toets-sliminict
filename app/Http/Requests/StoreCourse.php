<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCourse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fallback = config('app.fallback_locale');

        return [
            'name'                  => 'array',
            'description'           => 'array',
            "name.$fallback"        => 'required',
            "description.$fallback" => 'required',
            'lessons'               => 'required|numeric',
            'visible'               => 'boolean',
            'price'                 => 'required|numeric',
        ];
    }
}
