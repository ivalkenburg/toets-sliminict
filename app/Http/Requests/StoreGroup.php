<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreGroup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
                    'name'      => 'required',
                    'course_id' => 'required|exists:courses,id',
                    'starts_at' => 'required|date',
                    'ends_at'   => 'required|date',
                    'status'    => [
                        'required',
                        Rule::in(config('app.group_status')),
                    ],
                    'students'  => 'array',
                ];
            case 'POST':
                return [
                    'name'      => 'required|unique:groups,name',
                    'course_id' => 'required|exists:courses,id',
                    'starts_at' => 'required|date',
                    'ends_at'   => 'required|date',
                    'status'    => [
                        'required',
                        Rule::in(config('app.group_status')),
                    ],
                    'students'  => 'array',
                ];
            default:
                break;
        }
    }
}
