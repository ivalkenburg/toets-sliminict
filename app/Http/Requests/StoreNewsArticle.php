<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewsArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fallback = config('app.fallback_locale');

        return [
            'title'           => 'array',
            'description'     => 'array',
            "title.$fallback" => 'required',
            "body.$fallback"  => 'required',
        ];
    }
}
