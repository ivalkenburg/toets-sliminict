<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
                    'name'      => 'required',
                    'residence' => 'required',
                    'email'     => 'required|email',
                    'birthday'  => 'date',
                    'groups'    => 'array',
                    'picture'   => 'image|max:200|dimensions:ratio=1/1',
                ];
            case 'POST':
                return [
                    'name'      => 'required',
                    'residence' => 'required',
                    'email'     => 'required|email|unique:students,email',
                    'birthday'  => 'date',
                    'groups'    => 'array',
                    'picture'   => 'image|max:200|dimensions:ratio=1/1',
                ];
            default:
                break;
        }
    }
}
