<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $body;
    public $student;
    public $user;
    public $subject;

    public function __construct($subject, $body, $student, $user)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->student = $student;
        $this->user = $user;
    }

    public function build()
    {
        return $this->from($this->user)
                    ->subject($this->subject)
                    ->markdown('emails.student');
    }
}
