<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function getKeyName()
    {
        return 'key';
    }

    public function setKeyName($key)
    {
        return 'key';
    }

    public function getValueAttribute($value)
    {
        return unserialize($value);
    }

    public function setValueAttribute($value)
    {
        return $this->attributes['value'] = serialize($value);
    }

    public static function set($key, $value)
    {
        $meta = static::firstOrNew(['key' => $key]);

        $meta->value = $value;
        $meta->save();

        return $meta;
    }

    public function asLocale()
    {
        return $this->value[config('app.locale')] ?? $this->value[config('app.fallback_locale')];
    }
}
