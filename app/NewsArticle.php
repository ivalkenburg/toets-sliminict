<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsArticle extends Model
{
    protected $guarded = [];

    public $with = ['author'];

    public function getTitleAttribute($value)
    {
        return unserialize($value);
    }

    public function setTitleAttribute($value)
    {
        return $this->attributes['title'] = serialize($value);
    }

    public function getBodyAttribute($value)
    {
        return unserialize($value);
    }

    public function setBodyAttribute($value)
    {
        return $this->attributes['body'] = serialize($value);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function asLocale($key)
    {
        return $this->$key[config('app.locale')] ?? $this->$key[config('app.fallback_locale')];
    }

    public static function last()
    {
        return static::latest()->first();
    }
}
