<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class Student extends Model
{
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'birthday',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($student) {
            $student->changePicture();
        });
    }

    public function joinGroup($group)
    {
        return $this->groups()->attach($group);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)->withTimestamps();
    }

    public function leaveGroup($group)
    {
        return $this->groups()->detach($group);
    }

    public function syncGroups($groups)
    {
        return $this->groups()->sync($groups);
    }

    public function age()
    {
        return $this->birthday->age;
    }

    public function picture()
    {
        return $this->picture ? Storage::disk('public')->url($this->picture) : config('app.students_default_img');
    }

    public function addComment($request)
    {
        return $this->comments()->create([
            'author_id' => auth()->id(),
            'body'      => $request->body,
        ]);
    }

    public function comments()
    {
        return $this->hasMany(StudentComment::class)->orderBy('created_at', 'DESC');
    }

    public function changePicture($picture = null)
    {
        if ($this->picture) {
            Storage::disk('public')->delete($this->picture);
        }

        $this->picture = $picture;

        return $this;
    }

    public function sendMail(Mailable $mail)
    {
        return Mail::to($this)->send($mail);
    }
}
