<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentComment extends Model
{
    protected $with = ['author'];
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::created(function ($comment) {
            $comment->student()->touch();
        });
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }
}
