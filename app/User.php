<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token', 'activation_token',
    ];

    public function articles()
    {
        return $this->hasMany(NewsArticle::class);
    }

    public function studentComments()
    {
        return $this->hasMany(StudentComment::class, 'author_id');
    }

    public static function add($user)
    {
        return static::create([
            'name'             => $user['name'],
            'email'            => $user['email'],
            'activation_token' => md5(uniqid()),
        ]);
    }
}
