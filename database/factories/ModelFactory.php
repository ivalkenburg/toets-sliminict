<?php

use Carbon\Carbon;

/* @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\NewsArticle::class, function (Faker\Generator $faker) {
    $title = [];
    $body = [];

    foreach (config('app.locales') as $locale) {
        $title[$locale] = $faker->sentence;
        $body[$locale] = $faker->paragraphs(10, true);
    }

    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'title'   => $title,
        'body'    => $body,
    ];
});

$factory->define(App\Student::class, function (Faker\Generator $faker) {
    return [
        'name'      => $faker->name,
        'email'     => $faker->unique()->safeEmail,
        'birthday'  => $faker->date(),
        'residence' => $faker->city,
    ];
});

$factory->define(App\StudentComment::class, function (Faker\Generator $faker) {
    return [
        'student_id' => function () {
            return factory('App\Student')->create()->id;
        },
        'author_id'  => function () {
            return factory('App\User')->create()->id;
        },
        'body'       => $faker->paragraphs(4, true),
    ];
});

$factory->define(App\Course::class, function (Faker\Generator $faker) {
    $name = [];
    $description = [];

    foreach (config('app.locales') as $locale) {
        $name[$locale] = $faker->sentence(3);
        $description[$locale] = $faker->paragraphs(10, true);
    }

    return [
        'name'        => $name,
        'price'       => $faker->numberBetween(100, 10000),
        'lessons'     => $faker->numberBetween(5, 30),
        'description' => $description,
        'visible'     => true,
    ];
});

$factory->define(App\Group::class, function (Faker\Generator $faker) {
    $start = Carbon::instance($faker->dateTimeThisDecade());
    $end = Carbon::instance($start)->addWeeks($faker->numberBetween(5, 30));
    $status = config('app.group_status');

    return [
        'course_id' => function () {
            return factory('App\Course')->create()->id;
        },
        'name'      => strtoupper($faker->word).'-'.$faker->randomNumber(),
        'starts_at' => $start,
        'ends_at'   => $end,
        'comment'   => $faker->text,
        'status'    => $faker->randomElement($status),
    ];
});
