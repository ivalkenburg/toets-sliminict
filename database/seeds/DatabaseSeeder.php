<?php

use App\Meta;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $faker;

    public function __construct()
    {
        $this->faker = Faker\Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Users

        create('App\User', ['email' => 'lolrogii@gmail.com', 'password' => bcrypt('secret')]);
        create('App\User', ['email' => 'begeleider@sliminict.nl', 'password' => bcrypt('password')]);

        $users = create('App\User', [], 10)->pluck('id');

        for ($i = 0; $i < 40; $i++) {
            create('App\NewsArticle', ['user_id' => $this->faker->randomElement($users->toArray())]);
        }

        create('App\Course', [], 10)->each(function ($course) {
            create('App\Group', ['course_id' => $course->id], 1);
        });

        create('App\Course', ['visible' => false], 2);

        create('App\Student', [], 50);

        Meta::set('index_body', require 'index_body.php');
    }
}
