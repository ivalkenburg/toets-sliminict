window.$ = window.jQuery = require('jquery');
window.alertify = require('alertifyjs');

require('bootstrap-sass');

window.alert = function (data, alert) {
    var list = $(alert).children('ul');

    $(list).empty();

    $.each(data.responseJSON, function (index, value) {
        $(list).append('<li>' + value + '</li>');
    }.bind(list));

    $(alert).removeClass('hidden');
};