@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Add Course') }}</title>
@endsection

@section('content')
    <form action="{{ route('courses.store') }}" method="post">
        {{ csrf_field() }}
        <ul class="nav nav-tabs" role="tablist">

            @foreach($locales as $locale)
                <li role="presentation" {!! $locale == $fallback ? 'class="active"' : '' !!}>
                    <a role="tab" aria-controls="{{ $locale }}" data-toggle="tab" href="#{{ $locale }}"><span class="lang-xs lang-lbl" lang="{{ $locale }}"></span></a>
                </li>
            @endforeach

        </ul>
        <div class="tab-content">

            @foreach($locales as $locale)
                <div role="tabpanel" id="{{ $locale }}" class="tab-pane {{ $locale == $fallback ? 'active' : '' }}">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group{{ $errors->has("name.$locale") ? ' has-error' : '' }}">
                                <label for="name-{{ $locale }}">{{ __('Name') }}</label>
                                <input type="text" name="name[{{ $locale }}]" class="form-control" id="name-{{ $locale }}" placeholder="{{ __('Name') }}" value="{{ old("name.$locale") }}">
                                @if ($errors->has("name.$locale"))
                                    <span class="help-block">
                                        <strong>{{ ucfirst($errors->first("name.$locale")) }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has("description.$locale") ? ' has-error' : '' }}">
                                <label for="description-{{ $locale }}">{{ __('Description') }}</label>
                                <textarea class="form-control not-resizable" name="description[{{ $locale }}]" id="description-{{ $locale }}" rows="15" placeholder="{{ __('Description') }}">{{ old("description.$locale") }}</textarea>
                                @if ($errors->has("description.$locale"))
                                    <span class="help-block">
                                        <strong>{{ ucfirst($errors->first("description.$locale")) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            @component('layout.partials.panel')
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price">{{ __('Price') }}</label>
                            <div class="input-group">
                                <span class="input-group-addon">&euro;</span>
                                <input type="number" name="price" class="form-control" id="price" placeholder="{{ __('Price') }}" value="{{ old('price') }}">
                            </div>
                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ ucfirst($errors->first('price')) }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('lessons') ? ' has-error' : '' }}">
                            <label for="lessons">{{ __('Lessons') }}</label>
                            <input type="number" name="lessons" class="form-control" id="lessons" placeholder="{{ __('Lessons') }}" value="{{ old('lessons') }}">
                            @if ($errors->has('lessons'))
                                <span class="help-block">
                                    <strong>{{ ucfirst($errors->first('lessons')) }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('visible') ? ' has-error' : '' }}">
                    <label>{{ __('Visibility') }}</label>
                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                        <label class="btn btn-default active">
                            <input type="radio" name="visible" value="1" checked> {{ __('Visible') }}
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="visible" value="0"> {{  __('Invisible') }}
                        </label>
                    </div>
                    @if ($errors->has('visible'))
                        <span class="help-block">
                            <strong>{{ ucfirst($errors->first('visible')) }}</strong>
                        </span>
                    @endif
                </div>
            @endcomponent
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">{{ __('Add Course') }}</button>
        </div>
    </form>
@endsection

@include('layout.partials.ckeditor', ['textareas' => array_map(function($locale) { return "description[$locale]";}, $locales)])