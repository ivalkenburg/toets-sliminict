@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Edit Course') }} - {{ $course->asLocale('name') }}</title>
@endsection

@section('content')
    <form action="{{ route('courses.update', $course->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <ul class="nav nav-tabs" role="tablist">
            @foreach($locales as $locale)
                <li role="presentation" {!! $locale == $fallback ? 'class="active"' : '' !!}>
                    <a role="tab" aria-controls="{{ $locale }}" data-toggle="tab" href="#{{ $locale }}"><span class="lang-xs lang-lbl" lang="{{ $locale }}"></span></a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">

            @foreach($locales as $locale)
                <div role="tabpanel" id="{{ $locale }}" class="tab-pane {{ $locale == $fallback ? 'active' : '' }}">
                    @component('layout.partials.panel')
                        <div class="form-group{{ $errors->has("name.$locale") ? ' has-error' : '' }}">
                            <label for="name-{{ $locale }}">{{ __('Name') }}</label>
                            <input type="text" name="name[{{ $locale }}]" class="form-control" id="name-{{ $locale }}" placeholder="{{ __('Name') }}" value="{{ old("name.$locale", $course->name[$locale]) }}">
                            @if ($errors->has("name.$locale"))
                                <span class="help-block">
                                    <strong>{{ ucfirst($errors->first("name.$locale")) }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has("description.$locale") ? ' has-error' : '' }}">
                            <label for="description-{{ $locale }}">{{ __('Description') }}</label>
                            <textarea class="form-control not-resizable" name="description[{{ $locale }}]" id="description-{{ $locale }}" rows="15" placeholder="{{ __('Description') }}">{{ old("description.$locale", $course->description[$locale]) }}</textarea>
                            @if ($errors->has("description.$locale"))
                                <span class="help-block">
                                    <strong>{{ ucfirst($errors->first("description.$locale")) }}</strong>
                                </span>
                            @endif
                        </div>
                    @endcomponent
                </div>
            @endforeach

            @component('layout.partials.panel')
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price">{{ __('Price') }}</label>
                            <div class="input-group">
                                <span class="input-group-addon">&euro;</span>
                                <input type="number" name="price" class="form-control" id="price" placeholder="{{ __('Price') }}" value="{{ old('price', $course->price) }}">
                            </div>
                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ ucfirst($errors->first('price')) }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('lessons') ? ' has-error' : '' }}">
                            <label for="lessons">{{ __('Lessons') }}</label>
                            <input type="number" name="lessons" class="form-control" id="lessons" placeholder="{{ __('Lessons') }}" value="{{ old('lessons', $course->lessons) }}">
                            @if ($errors->has('lessons'))
                                <span class="help-block">
                                    <strong>{{ ucfirst($errors->first('lessons')) }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('visible') ? ' has-error' : '' }}">
                    <label>{{ __('Visibility') }}</label>
                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                        <label class="btn btn-default {{ $course->visible ? 'active' : '' }}">
                            <input type="radio" name="visible" value="1" {{ $course->visible ? 'checked' : '' }}> {{ __('Visible') }}
                        </label>
                        <label class="btn btn-default {{ $course->visible ?: 'active' }}">
                            <input type="radio" name="visible" value="0" {{ $course->visible ?: 'checked' }}> {{  __('Invisible') }}
                        </label>
                    </div>
                    @if ($errors->has('visible'))
                        <span class="help-block">
                            <strong>{{ ucfirst($errors->first('visible')) }}</strong>
                        </span>
                    @endif
                </div>
            @endcomponent

        </div>
        <div class="text-right">
            <button id="delete-button" class="btn btn-danger">{{ __('Delete') }}</button>
            <button type="submit" class="btn btn-primary">{{ __('Edit Course') }}</button>
        </div>
    </form>
    <form id="delete-form" method="post" action="{{ route('courses.destroy', $course->id) }}">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@endsection

@include('layout.partials.ckeditor', ['textareas' => array_map(function($locale) { return "description[$locale]";}, $locales)])

@push('js')
    <script>
        $('#delete-button').click(function(e) {
            e.preventDefault();

            if(confirm('{{ __('Delete Course') }}?')) {
                $('#delete-form').submit();
            }
        });
    </script>
@endpush