@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Courses') }}</title>
@endsection

@section('content')
    <div class="mb10 text-right">
        <a href="{{ route('courses.create') }}" target="_self">
            <button class="btn btn-primary">{{ __('Add Course') }}</button>
        </a>
    </div>

    @component('layout.partials.panel')
        <table id="courses" class="table table-striped table-hover table-responsive">
            <thead>
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Price') }}</th>
                    <th>{{ __('Lessons') }}</th>
                    <th>{{ __('Groups') }}</th>
                    <th>{{ __('Created at') }}</th>
                    <th>{{ __('Updated at') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($courses as $course)
                    <tr>
                        <td>{{ $course->asLocale('name') }}</td>
                        <td>&euro;{{ $course->price(false) }}</td>
                        <td>{{ $course->lessons }}</td>
                        <td><a href="{{ route('groups.index') }}?course={{ $course->id }}">{{ $course->groups_count }}</a></td>
                        <td>{{ $course->updated_at }}</td>
                        <td>{{ $course->created_at }}</td>
                        <td class="text-center">
                            <a href="{{ route('courses.edit', $course->id) }}" target="_self" title="{{ __('Visibility') }}"><span class="glyphicon glyphicon-{{ $course->visible ? 'eye-open text-primary' : 'eye-close text-muted' }}" aria-hidden="true"></span></a>
                            <a href="{{ route('courses.edit', $course->id) }}" target="_self" title="{{ __('Edit') }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                            <a class="delete" href="#delete" data-id="{{ $course->id }}" target="_self" title="{{ __('Delete') }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endcomponent
@endsection

@include('layout.partials.datatables', ['table' => '#courses', 'features' => true])

@push('js')
    <script>
        $('.delete').click(function(e) {
            e.preventDefault();

            if (confirm('{{ __('Delete Course') }}?')) {
                $.ajax({
                    url: '/admin/courses/' + $(this).data('id'),
                    method: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        '_method': 'DELETE'
                    }
                }).done(function(data) {
                    this.closest('tr').remove();
                    alertify.notify(data.message, data.status);
                }.bind(this));
            }
        })
    </script>
@endpush