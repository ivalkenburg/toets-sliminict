@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Add Group') }}</title>
@endsection

@section('content')
    <form action="{{ route('groups.store') }}" method="POST">
        {{ csrf_field() }}
        @component('layout.partials.panel')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('Name') }}" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('name')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }}">
                        <label for="course_id">{{ __('Course') }}</label>
                        <select name="course_id" id="course_id" class="form-control">
                            <option disabled{{ old('course_id') ? '' : ' selected' }}>{{ __('Select a course') }}</option>
                            @foreach($courses as $course)
                                <option value="{{ $course->id }}" {{ old('course_id') == $course->id ? 'selected' : '' }}>{{ $course->asLocale('name') }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('course_id'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('course_id')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('starts_at') ? ' has-error' : '' }}">
                        <label for="starts_at">{{ __('Starts on') }}</label>
                        <input type="date" name="starts_at" class="form-control" id="starts_at" placeholder="{{ __('Starts on') }}" value="{{ old('starts_at') }}">
                        @if ($errors->has('starts_at'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('starts_at')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('ends_at') ? ' has-error' : '' }}">
                        <label for="ends_at">{{ __('Ends on') }}</label>
                        <input type="date" name="ends_at" class="form-control" id="ends_at" placeholder="{{ __('Ends on') }}" value="{{ old('ends_at') }}">
                        @if ($errors->has('ends_at'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('ends_at')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="comment">{{ __('Comment') }}</label>
                        <textarea name="comment" id="comment" rows="5" class="form-control not-resizable" placeholder="{{ __('Comment') }}">{{ old('comment') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status">{{ __('Group Status') }}</label>
                        <select name="status" id="status" class="form-control">
                            <option disabled{{ old('status') ? '' : ' selected' }}>{{ __('Select a status') }}</option>
                            @foreach(config('app.group_status') as $status)
                                <option value="{{ $status }}" {{ old('status') == $status ? 'selected' : '' }}>{{ __(ucfirst($status)) }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('status'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('status')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        @endcomponent
        @component('layout.partials.panel')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="students">{{ __('Available Students') }}</label>
                        <select id="students" class="form-control" size="10" multiple>
                            @foreach($students as $student)
                                @if(!in_array($student->id, old('students', [])))
                                    <option value="{{ $student->id }}">{{ $student->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="students_to">{{ __('Selected Students') }}</label>
                        <select name="students[]" id="students_to" class="form-control" size="10" multiple>
                            @foreach($students as $student)
                                @if(in_array($student->id, old('students', [])))
                                    <option value="{{ $student->id }}">{{ $student->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        @endcomponent
        <div class="text-right">
            <button class="btn btn-primary" type="submit">{{ __('Add Group') }}</button>
        </div>
    </form>
@endsection

@include('layout.partials.multiselect', ['multiselect' => '#students'])
@include('layout.partials.datepicker', ['format' => 'yyyy-mm-dd'])