@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Edit Group') }} - {{ $group->name }}</title>
@endsection

@section('content')
    <form action="{{ route('groups.update', $group->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        @component('layout.partials.panel')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('Name') }}" value="{{ old('name', $group->name) }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('name')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }}">
                        <label for="course_id">{{ __('Course') }}</label>
                        <select name="course_id" id="course_id" class="form-control">
                            <option disabled{{ old('course_id', $group->course_id) ? '' : ' selected' }}>{{ __('Select a course') }}</option>
                            @foreach($courses as $course)
                                <option value="{{ $course->id }}" {{ old('course_id', $group->course_id) == $course->id ? 'selected' : '' }}>{{ $course->asLocale('name') }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('course_id'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('course_id')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('starts_at') ? ' has-error' : '' }}">
                        <label for="starts_at">{{ __('Starts on') }}</label>
                        <input type="date" name="starts_at" class="form-control" id="starts_at" placeholder="{{ __('Starts on') }}" value="{{ old('starts_at', $group->starts_at->toDateString()) }}">
                        @if ($errors->has('starts_at'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('starts_at')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('ends_at') ? ' has-error' : '' }}">
                        <label for="ends_at">{{ __('Ends on') }}</label>
                        <input type="date" name="ends_at" class="form-control" id="ends_at" placeholder="{{ __('Ends on') }}" value="{{ old('ends_at', $group->ends_at->toDateString()) }}">
                        @if ($errors->has('ends_at'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('ends_at')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="comment">{{ __('Comment') }}</label>
                        <textarea name="comment" id="comment" rows="5" class="form-control not-resizable" placeholder="{{ __('Comment') }}">{{ old('comment', $group->comment) }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status">{{ __('Group Status') }}</label>
                        <select name="status" id="status" class="form-control">
                            <option disabled{{ old('status', $group->status) ? '' : ' selected' }}>{{ __('Select a status') }}</option>
                            @foreach(config('app.group_status') as $status)
                                <option value="{{ $status }}" {{ old('status', $group->status) == $status ? 'selected' : '' }}>{{ __(ucfirst($status)) }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('status'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('status')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        @endcomponent
        @component('layout.partials.panel')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="students">{{ __('Available Students') }}</label>
                        <select id="students" class="form-control" size="10" multiple>
                            @foreach($students as $student)
                                @if(!in_array($student->id, old('students', $group->students->pluck('id')->toArray())))
                                    <option value="{{ $student->id }}">{{ $student->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="students_to">{{ __('Selected Students') }}</label>
                        <select name="students[]" id="students_to" class="form-control" size="10" multiple>
                            @foreach($students as $student)
                                @if(in_array($student->id, old('students', $group->students->pluck('id')->toArray())))
                                    <option value="{{ $student->id }}">{{ $student->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        @endcomponent
        <div class="text-right">
            <button id="delete-button" class="btn btn-danger">{{ __('Delete') }}</button>
            <button class="btn btn-primary" type="submit">{{ __('Edit Group') }}</button>
        </div>
    </form>
    <form id="delete-form" action="{{ route('groups.destroy', $group->id) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@endsection

@push('js')
    <script>
        $('#delete-button').click(function(e) {
            e.preventDefault();

            if(confirm('{{ __('Delete Group') }}?')) {
                $('#delete-form').submit();
            }
        });
    </script>
@endpush

@include('layout.partials.multiselect', ['multiselect' => '#students'])
@include('layout.partials.datepicker', ['format' => 'yyyy-mm-dd'])