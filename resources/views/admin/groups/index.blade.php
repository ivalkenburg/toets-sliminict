@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Groups') }}</title>
@endsection

@section('content')
    <div class="mb10 text-right">
        <a href="{{ route('groups.create') }}">
            <button class="btn btn-primary">{{ __('Add Group') }}</button>
        </a>
    </div>

    @component('layout.partials.panel')
        <table id="groups" class="table table-striped table-hover table-responsive">
            <thead>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Course') }}</th>
                <th>{{ __('Students') }}</th>
                <th>{{ __('Status') }}</th>
                <th>{{ __('Created at') }}</th>
                <th>{{ __('Updated at') }}</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($groups as $group)
                    <tr>
                        <td><a href="{{ route('groups.show', $group->id) }}">{{ $group->name }}</a></td>
                        <td><a href="{{ route('courses.edit', $group->course_id) }}">{{ $group->course->asLocale('name') }}</a></td>
                        <td>{{ $group->students_count }}</td>
                        <td>{{ __(ucfirst($group->status)) }}</td>
                        <td>{{ $group->created_at }}</td>
                        <td>{{ $group->updated_at }}</td>
                        <td class="text-center">
                            <a href="{{ route('groups.edit', $group->id) }}" title="{{ __('Edit') }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                            <a class="delete" href="#delete" data-id="{{ $group->id }}" title="{{ __('Delete') }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endcomponent
@endsection

@include('layout.partials.datatables', ['table' => '#groups', 'features' => true])

@push('js')
<script>
    $('.delete').click(function (e) {
        e.preventDefault();

        if (confirm('{{ __('Delete Group') }}?')) {
            $.ajax({
                url: '/admin/groups/' + $(this).data('id'),
                method: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    '_method': 'DELETE'
                }
            }).done(function (data) {
                $(this).closest('tr').remove();
                alertify.notify(data.message, data.status);
            }.bind(this))
        }
    });
</script>
@endpush