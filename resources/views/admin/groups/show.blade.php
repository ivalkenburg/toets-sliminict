@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ $group->name }}</title>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-3">
            @component('layout.partials.panel')
                @slot('heading')
                    <span class="is-bold">{{ $group->name }}</span>
                @endslot
                <div class="list-group mb10">
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Course') }}</h5>
                        <p class="list-group-item-text">
                            <a href="{{ route('courses.edit', $group->course->id) }}">{{ $group->course->asLocale('name') }}</a>
                        </p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Starts on') }}</h5>
                        <p class="list-group-item-text">
                            {{ $group->starts_at->toFormattedDateString() }}
                        </p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Ends on') }}</h5>
                        <p class="list-group-item-text">
                            {{ $group->ends_at->toFormattedDateString() }}
                        </p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Duration') }}</h5>
                        <p class="list-group-item-text">
                            {{ $group->ends_at->diffForHumans($group->starts_at, true) }}
                        </p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Group Status') }}</h5>
                        <p class="list-group-item-text">
                            {{ ucfirst($group->status) }}
                        </p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Number of students') }}</h5>
                        <p class="list-group-item-text">
                            {{ $group->students->count() }}
                        </p>
                    </div>
                </div>
                <div>
                    <div class="mb10">
                        <a class="btn btn-primary btn-block" href="{{ route('groups.edit', $group->id) }}">{{ __('Edit Group') }}</a>
                    </div>
                    <form action="{{ route('groups.destroy', $group->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger btn-block">{{ __('Delete Group') }}</button>
                    </form>
                </div>
            @endcomponent
        </div>
        <div class="col-sm-9">
            @if($group->comment)
                @component('layout.partials.panel')
                    @slot('heading')
                        <span class="is-bold">{{ __('Comment') }}</span>
                    @endslot
                    {{ $group->comment }}
                @endcomponent
            @endif
            @component('layout.partials.panel')
                @slot('heading')
                    <span class="is-bold">{{ __('Students') }}</span>
                @endslot
                <table id="students" class="table table-striped table-hover">
                    <thead>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('E-Mail Address') }}</th>
                        <th>{{ __('Added on') }}</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach($group->students as $student)
                            <tr>
                                <td><a href="{{ route('students.show', $student->id) }}">{{ $student->name }}</a></td>
                                <td><a href="{{ route('students.email', $student->id) }}">{{ $student->email }}</a></td>
                                <td>{{ $student->pivot->created_at }}</td>
                                <td class="text-center">
                                    <a class="detach" data-id="{{ $student->id }}" href="#detach" title="{{ __('Remove from group') }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endcomponent
        </div>
    </div>
@endsection

@include('layout.partials.datatables', ['table' => '#students', 'features' => false])

@push('js')
    <script>
        $('.detach').click(function (e) {
            e.preventDefault();

            if (confirm('{{ __('Remove student from group') }}?')) {
                $.ajax({
                    url: '/admin/students/' + $(this).data('id') + '/leave/{{ $group->id }}',
                    method: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}'
                    }
                }).done(function (data) {
                    $(this).closest('tr').remove();
                    alertify.notify(data.message, data.status)
                }.bind(this))
            }
        });
    </script>
@endpush