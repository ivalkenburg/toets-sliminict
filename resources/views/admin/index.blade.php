@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Admin Panel') }}</title>
@endsection

@section('content')
    <form action="{{ url('/admin/store') }}" method="post">
        {{ csrf_field() }}
        <ul class="nav nav-tabs" role="tablist">

            @foreach($locales as $locale)
                <li role="presentation" {!! $locale == $fallback ? 'class="active"' : '' !!}>
                    <a role="tab" aria-controls="{{ $locale }}" data-toggle="tab" href="#{{ $locale }}"><span class="lang-xs lang-lbl" lang="{{ $locale }}"></span></a>
                </li>
            @endforeach

        </ul>
        <div class="tab-content">

            @foreach($locales as $locale)
                <div role="tabpanel" id="{{ $locale }}" class="tab-pane {{ $locale == $fallback ? 'active' : '' }}">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="body-{{ $locale }}">Index {{ __('Content') }}</label>
                                <textarea class="form-control not-resizable" name="body[{{ $locale }}]" id="body-{{ $locale }}" rows="15" placeholder="{{ __('Content') }}">{{ $body->value[$locale] ?? '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">{{ __('Publish') }}</button>
        </div>
    </form>
@endsection

@include('layout.partials.ckeditor', ['textareas' => array_map(function($locale) { return "body[$locale]";}, $locales)])