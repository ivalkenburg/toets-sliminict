@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('New Article') }}</title>
@endsection

@section('content')
    <form action="{{ route('news.store') }}" method="post">
        {{ csrf_field() }}
        <ul class="nav nav-tabs" role="tablist">

            @foreach($locales as $locale)
                <li role="presentation" {!! $locale == $fallback ? 'class="active"' : '' !!}>
                    <a role="tab" aria-controls="{{ $locale }}" data-toggle="tab" href="#{{ $locale }}"><span class="lang-xs lang-lbl" lang="{{ $locale }}"></span></a>
                </li>
            @endforeach

        </ul>
        <div class="tab-content">

            @foreach($locales as $locale)
                <div role="tabpanel" id="{{ $locale }}" class="tab-pane {{ $locale == $fallback ? 'active' : '' }}">
                    @component('layout.partials.panel')
                        <div class="form-group{{ $errors->has("title.$locale") ? ' has-error' : '' }}">
                            <label for="title-{{ $locale }}">{{ __('Title') }}</label>
                            <input type="text" name="title[{{ $locale }}]" class="form-control" id="title-{{ $locale }}" placeholder="{{ __('Title') }}" value="{{ old("title.$locale") }}">
                            @if ($errors->has("title.$locale"))
                                <span class="help-block">
                                    <strong>{{ ucfirst($errors->first("title.$locale")) }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has("body.$locale") ? ' has-error' : '' }}">
                            <label for="body-{{ $locale }}">{{ __('Content') }}</label>
                            <textarea class="form-control not-resizable" name="body[{{ $locale }}]" id="body-{{ $locale }}" rows="15" placeholder="{{ __('Content') }}">{{ old("body.$locale") }}</textarea>
                            @if ($errors->has("body.$locale"))
                                <span class="help-block">
                                    <strong>{{ ucfirst($errors->first("body.$locale")) }}</strong>
                                </span>
                            @endif
                        </div>
                    @endcomponent
                </div>
            @endforeach

        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">{{ __('Publish') }}</button>
        </div>
    </form>
@endsection

@include('layout.partials.ckeditor', ['textareas' => array_map(function($locale) { return "body[$locale]";}, $locales)])