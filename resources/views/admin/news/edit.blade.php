@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Edit Article') }} - {{ $article->asLocale('title') }}</title>
@endsection

@section('content')
    <form action="{{ route('news.update', $article->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <ul class="nav nav-tabs" role="tablist">

            @foreach($locales as $locale)
                <li role="presentation" {!! $locale == $fallback ? 'class="active"' : '' !!}>
                    <a role="tab" aria-controls="{{ $locale }}" data-toggle="tab" href="#{{ $locale }}"><span class="lang-xs lang-lbl" lang="{{ $locale }}"></span></a>
                </li>
            @endforeach

        </ul>
        <div class="tab-content">

            @foreach($locales as $locale)
                <div role="tabpanel" id="{{ $locale }}" class="tab-pane {{ $locale == $fallback ? 'active' : '' }}">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group{{ $errors->has("title.$locale") ? ' has-error' : '' }}">
                                <label for="title-{{ $locale }}">{{ __('Title') }}</label>
                                <input type="text" name="title[{{ $locale }}]" class="form-control" id="title-{{ $locale }}" placeholder="{{ __('Title') }}" value="{{ old("title.$locale", $article->title[$locale]) }}">
                                @if ($errors->has("title.$locale"))
                                    <span class="help-block">
                                        <strong>{{ ucfirst($errors->first("title.$locale")) }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has("body.$locale") ? ' has-error' : '' }}">
                                <label for="body-{{ $locale }}">{{ __('Content') }}</label>
                                <textarea class="form-control not-resizable" name="body[{{ $locale }}]" id="body-{{ $locale }}" rows="15" placeholder="{{ __('Content') }}">{{ old("body.$locale", $article->body[$locale]) }}</textarea>
                                @if ($errors->has("body.$locale"))
                                    <span class="help-block">
                                        <strong>{{ ucfirst($errors->first("body.$locale")) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="text-right">
            <button id="delete-button" class="btn btn-danger">{{ __('Delete') }}</button>
            <button type="submit" class="btn btn-primary">{{ __('Edit Article') }}</button>
        </div>
    </form>
    <form id="delete-form" action="{{ route('news.destroy', $article->id) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@endsection

@include('layout.partials.ckeditor', ['textareas' => array_map(function($locale) { return "body[$locale]";}, $locales)])

@push('js')
<script>
    $('#delete-button').click(function (e) {
        e.preventDefault();

        if (confirm('{{ __('Delete Article') }}?')) {
            $('#delete-form').submit();
        }
    });
</script>
@endpush
