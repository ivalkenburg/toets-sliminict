@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('News Articles') }}</title>
@endsection

@section('content')
    <div class="mb10 text-right">
        <a href="{{ route('news.create') }}" target="_self">
            <button class="btn btn-primary">{{ __('New Article') }}</button>
        </a>
    </div>

    @component('layout.partials.panel')
        <table id="articles" class="table table-striped table-hover table-responsive">
            <thead>
                <tr>
                    <th>{{ __('Title') }}</th>
                    <th>{{ __('Author') }}</th>
                    <th>{{ __('Updated at') }}</th>
                    <th>{{ __('Created at') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td>
                            <a href="{{ route('news.show', $article->id) }}">{{ $article->asLocale('title') }}</a>
                        </td>
                        <td>{{ $article->author->name }}</td>
                        <td>{{ $article->updated_at }}</td>
                        <td>{{ $article->created_at }}</td>
                        <td class="text-center">
                            <a href="{{ route('news.edit', $article->id) }}" target="_self" title="{{ __('Edit') }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                            <a class="delete" href="#" data-id="{{ $article->id }}" target="_self" title="{{ __('Delete') }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endcomponent
@endsection

@include('layout.partials.datatables', ['table' => '#articles', 'features' => true])

@push('js')
<script>
    $('.delete').click(function (e) {
        e.preventDefault();

        if (confirm('{{ __('Delete Article') }}?')) {
            $.ajax({
                url: '/admin/news/' + $(this).data('id'),
                method: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    '_method': 'DELETE'
                }
            }).done(function (data) {
                $(this).closest('tr').remove();
                alertify.notify(data.message, data.status);
            }.bind(this))
        }
    });
</script>
@endpush