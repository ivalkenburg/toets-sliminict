@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Edit Student') }} - {{ $student->id }}</title>
@endsection

@section('content')
    <form action="{{ route('students.update', $student->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        @component('layout.partials.panel')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('Name') }}" value="{{ old('name', $student->name) }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('name')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('residence') ? ' has-error' : '' }}">
                        <label for="residence">{{ __('Residence') }}</label>
                        <input type="text" name="residence" class="form-control" id="residence" placeholder="{{ __('Residence') }}" value="{{ old('residence', $student->residence) }}">
                        @if ($errors->has('residence'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('residence')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email', $student->email) }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('email')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                        <label for="birthday">{{ __('Day of birth') }}</label>
                        <input type="date" name="birthday" class="form-control" id="birthday" placeholder="{{ __('Day of birth') }}" value="{{ old('birthday', $student->birthday->toDateString()) }}">
                        @if ($errors->has('birthday'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('birthday')) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        @endcomponent
        @component('layout.partials.panel')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="groups">{{ __('Available Groups') }}</label>
                        <select id="groups" class="form-control" size="10" multiple>
                            @foreach($groups as $group)
                                @if(!in_array($group->id, old('groups', $student->groups->pluck('id')->toArray())))
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="groups_to">{{ __('Selected Groups') }}</label>
                        <select name="groups[]" id="groups_to" class="form-control" size="10" multiple>
                            @foreach($groups as $group)
                                @if(in_array($group->id, old('groups', $student->groups->pluck('id')->toArray())))
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        @endcomponent
        @component('layout.partials.panel')
            <div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
                <label for="picture">{{ __('Picture') }}</label>
                <input type="file" class="filestyle" name="picture" id="picture">
                @if ($errors->has('picture'))
                    <span class="help-block">
                        <strong>{{ ucfirst($errors->first('picture')) }}</strong>
                    </span>
                @else
                    <span class="help-block">{{ __('Picture must be 1/1 ratio and max 200kb.') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label class="checkbox-inline"><input type="checkbox" name="no_picture" value="1"{{ old('no_picture') ? ' checked' : '' }}>{{ __('Delete profile picture') }}</label>
            </div>
        @endcomponent
        <div class="text-right">
            <button id="delete-button" class="btn btn-danger">{{ __('Delete') }}</button>
            <button class="btn btn-primary" type="submit">{{ __('Edit Student') }}</button>
        </div>
    </form>
    <form id="delete-form" action="{{ route('students.destroy', $student->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@endsection

@include('layout.partials.multiselect', ['multiselect' => '#groups'])
@include('layout.partials.datepicker', ['format' => 'yyyy-mm-dd'])
@include('layout.partials.filestyle')

@push('js')
    <script>
        $('#delete-button').click(function(e) {
            e.preventDefault();

            if(confirm('{{ __('Delete Student') }}?')) {
                $('#delete-form').submit();
            }
        })
    </script>
@endpush

