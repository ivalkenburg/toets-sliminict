@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Send Mail') }} - {{ $student->name }}</title>
@endsection

@section('content')
    <form action="{{ route('students.email', $student->id) }}" method="POST">
        {{ csrf_field() }}
        @component('layout.partials.panel')
            @slot('heading')
                <span class="is-bold">{{ __('Send email to') }} {{ $student->name }}</span>
            @endslot
            <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                <label for="subject">{{ __('Subject') }}</label>
                <input type="text" name='subject' class="form-control" id="subject" placeholder="{{ __('Subject') }}" value="{{ old('subject') }}">
                @if($errors->has('subject'))
                    <span class="help-block">
                        <strong>{{ ucfirst($errors->first('subject')) }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                <label for="body">{{ __('Content') }}</label>
                <textarea class="form-control not-resizable" name="body" id="body" rows="15" placeholder="{{ __('Content') }}">{{ old('body') }}</textarea>
                @if($errors->has('body'))
                    <span class="help-block">
                        <strong>{{ ucfirst($errors->first('body')) }}</strong>
                    </span>
                @endif
            </div>
        @endcomponent
        <div class="text-right">
            <button type="submit" class="btn btn-primary">{{ __('Send Email') }}</button>
        </div>
    </form>
@endsection
