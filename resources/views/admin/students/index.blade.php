@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Students') }}</title>
@endsection

@section('content')
    <div class="mb10 text-right">
        <a href="{{ route('students.create') }}">
            <button class="btn btn-primary">{{ __('Add Student') }}</button>
        </a>
    </div>

    @component('layout.partials.panel')
        <table id="students" class="table table-striped table-hover table-responsive">
            <thead>
                <th>{{ __('Name') }}</th>
                <th>{{ __('E-Mail Address') }}</th>
                <th>{{ __('Age') }}</th>
                <th>{{ __('Residence') }}</th>
                <th>{{ __('Created at') }}</th>
                <th>{{ __('Updated at') }}</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($students as $student)
                    <tr>
                        <td><a href="{{ route('students.show', $student->id) }}">{{ $student->name }}</a></td>
                        <td><a href="{{ route('students.email', $student->id) }}">{{ $student->email }}</a></td>
                        <td><span title="{{ $student->birthday->toFormattedDateString() }}">{{ $student->age() }}</span></td>
                        <td>{{ $student->residence }}</td>
                        <td>{{ $student->created_at }}</td>
                        <td>{{ $student->updated_at }}</td>
                        <td class="text-center">
                            <a href="{{ route('students.edit', $student->id) }}" title="{{ __('Edit') }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                            <a class="delete" href="#" data-id="{{ $student->id }}" title="{{ __('Delete') }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endcomponent
@endsection

@include('layout.partials.datatables', ['table' => '#students', 'features' => true])

@push('js')
<script>
    $('.delete').click(function (e) {
        e.preventDefault();

        if (confirm('{{ __('Delete Student') }}?')) {
            $.ajax({
                url: '/admin/students/' + $(this).data('id'),
                method: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    '_method': 'DELETE'
                }
            }).done(function (data) {
                $(this).closest('tr').remove();
                alertify.notify(data.message, data.status);
            }.bind(this))
        }
    });
</script>
@endpush