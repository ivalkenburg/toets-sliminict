@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ $student->name }}</title>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4 col-md-3">
            @component('layout.partials.panel')
                @slot('heading')
                    <span class="is-bold">{{ $student->name }}</span>
                @endslot
                <div class="mb10">
                    <img src="{{ $student->picture() }}" alt="{{ $student->name }}" class="img-thumbnail" width="100%" height="100%">
                </div>
                <div class="list-group mb10">
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Day of birth') }}</h5>
                        <p class="list-group-item-text">{{ $student->birthday->toDateString() }} ({{ $student->age() }})</p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Residence') }}</h5>
                        <p class="list-group-item-text">{{ $student->residence }}</p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('E-Mail Address') }}</h5>
                        <p class="list-group-item-text"><a href="{{ route('students.email', $student->id) }}">{{ $student->email }}</a></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Created at') }}</h5>
                        <p class="list-group-item-text">{{ $student->created_at->toDayDateTimeString() }}</p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Updated at') }}</h5>
                        <p class="list-group-item-text">{{ $student->updated_at->toDayDateTimeString() }}</p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading is-bold">{{ __('Groups') }}</h5>
                        @forelse($student->groups as $group)
                            <li><a href="{{ route('groups.show', $group->id) }}">{{ $group->name }}</a></li>
                        @empty
                            <li>{{ __('No groups') }}</li>
                        @endforelse
                    </div>
                </div>
                <div>
                    <div class="mb10">
                        <a class="btn btn-primary btn-block" href="{{ route('students.edit', $student->id) }}">{{ __('Edit Student') }}</a>
                    </div>
                    <form action="{{ route('students.destroy', $student->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button id="delete" type="submit" class="btn btn-danger btn-block">{{ __('Delete Student') }}</button>
                    </form>
                </div>
            @endcomponent
        </div>
        <div class="col-sm-8 col-md-9">
            @component('layout.partials.panel')
                @slot('heading')
                    <span class="is-bold">{{ __('Comment') }}</span>
                @endslot
                <form action="{{ route('students.comments', $student->id) }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                        <textarea class="form-control not-resizable" name="body" id="body" rows="5" placeholder="{{ __('Comment') }}">{{ old('body') }}</textarea>
                        @if ($errors->has('body'))
                            <span class="help-block">
                                <strong>{{ ucfirst($errors->first('body')) }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="text-right">
                        <button class="btn btn-primary">{{ __('Post Comment') }}</button>
                    </div>
                </form>
            @endcomponent
            @foreach($student->comments as $comment)
                @component('layout.partials.panel', ['id' => "comment-{$comment->id}"])
                    @slot('heading')
                        <span class="is-bold">{{ $comment->author->name }}</span> {{ __('wrote') }} {{ $comment->created_at->diffForHumans() }}.
                    @endslot
                    {{ $comment->body }}
                @endcomponent
            @endforeach
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('#delete').click(function(e) {
            e.preventDefault();

            if (confirm('{{ __('Delete Student') }}?')) {
                $(this).closest('form').submit();
            }
        })
    </script>
@endpush