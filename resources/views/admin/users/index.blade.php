@extends('layout.admin')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Users') }}</title>
@endsection

@section('content')
    <div class="mb10 text-right">
        <button class="btn btn-primary" data-toggle="modal" data-target="#adduser">{{ __('Add User') }}</button>
    </div>
    @component('layout.partials.panel')
        <table id="users" class="table table-striped table-hover table-responsive">
            <thead>
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('E-Mail Address') }}</th>
                    <th>{{ __('Updated at') }}</th>
                    <th>{{ __('Created at') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            {{ $user->name }}
                            @isset($user->activation_token)
                                <span class="label label-default">{{ __('Inactive') }}</span>
                            @endisset
                        </td>
                        <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                        <td>{{ $user->updated_at }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td class="text-center">
                            <a class="delete" href="#" data-id="{{ $user->id }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endcomponent

    <div id="adduser" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ __('Add User') }}</h4>
                </div>
                <form action="{{ route('users.store') }}" method="POST">
                    <div class="modal-body">
                        <div class="alert alert-danger hidden alert-dismissible">
                            <ul></ul>
                        </div>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="control-label">{{ __('Name') }}</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('Name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">{{ __('E-Mail Address') }}</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="{{ __('E-Mail Address') }}" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
                        <button type="submit" data-loading-text="{{ __('Sending') }}..." class="btn btn-primary">{{ __('Send Activation Email') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@include('layout.partials.datatables', ['table' => '#users', 'features' => true])

@push('js')
    <script>
        $('.delete').click(function (e) {
            e.preventDefault();

            if (confirm('{{ __('Delete User') }}?')) {
                $.ajax({
                    url: '/admin/users/' + $(this).data('id'),
                    method: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        '_method': 'DELETE'
                    }
                }).done(function (data) {
                    $(this).closest('tr').remove();
                    alertify.notify(data.message, data.status)
                }.bind(this))
            }
        });

        $('#adduser').find('form').submit(function (e) {
            e.preventDefault();

            var btn = $(this).find('button[type=submit]');

            $.ajax({
                url: '{{ route('users.store') }}',
                method: 'POST',
                data: $(this).serialize(),
                beforeSend: function () {
                    $(btn).attr('disabled', 'disabled').button('loading');
                }.bind(btn)
            }).done(function () {
                location.reload();
            }).fail(function (data) {
                alert(data, $(this).find('.alert'));
            }.bind(this)).always(function () {
                $(btn).removeAttr('disabled').button('reset');
            }.bind(btn))
        })
    </script>
@endpush