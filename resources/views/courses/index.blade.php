@extends('layout.public')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('Courses') }}</title>
@endsection

@section('content')
    @foreach($courses as $course)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="is-marginless text-primary">{{ $course->asLocale('name') }}</h2>
            </div>
            <div class="panel-body">
                {!! $course->asLocale('description') !!}
            </div>
            <div class="panel-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-6"><strong>{{ __('Price') }}:</strong>&euro;{{ $course->price() }} <small>({{ __('Incl. VAT') }})</small></div>
                        <div class="col-xs-6"><strong>{{ __('Number of lessons') }}:</strong> {{ $course->lessons }}</div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection