@component('mail::message')
# Activation Required

Hello {{ $user->name }},
your account is current disabled. To activate it click the link below to set your password and activate your account.

@component('mail::button', ['url' => route('users.activateForm', $user->activation_token)])
Activate Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
