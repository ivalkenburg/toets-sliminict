@extends('layout.public')

@section('meta')
    <title>{{ config('app.name') }}</title>
@endsection

@section('content')
    @isset($body)
        <section>
            {!! $body->asLocale() ?? '' !!}
        </section>
    @endisset

    @isset($news)
        @include('layout.partials.article', ['article' => $news])
    @endisset
@endsection