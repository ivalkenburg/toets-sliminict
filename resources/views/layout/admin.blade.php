<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @stack('css')
</head>
<body>
    <header>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('home') }}">
                        {{ config('app.name') }}
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('news.index') }}">{{ __('News') }}</a></li>
                        <li><a href="{{ route('users.index') }}">{{ __('Users') }}</a></li>
                        <li><a href="{{ route('courses.index') }}">{{ __('Courses') }}</a></li>
                        <li><a href="{{ route('groups.index') }}">{{ __('Groups') }}</a></li>
                        <li><a href="{{ route('students.index') }}">{{ __('Students') }}</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if(auth()->check())
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ auth()->user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('logout') }}">{{ __('Logout') }}</a></li>
                                </ul>
                            </li>
                        @else
                            <li><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="lang-xs" lang="{{ config('app.locale') }}"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach(config('app.locales') as $locale)
                                    <li><a href="{{ request()->fullUrlWithQuery(['lang' => $locale]) }}"><span class="lang-xs lang-lbl" lang="{{ $locale }}"></span></a></li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main class="container">
        @yield('content')
    </main>

    @include('layout.partials.footer')

    <script src="{{ mix('js/app.js') }}"></script>

    @include('layout.partials.alertify')
    @stack('js')
</body>
</html>
