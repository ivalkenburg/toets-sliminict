@if(count($errors))
    <ul class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        @foreach($errors->all() as $error)
            <li>{{ ucfirst($error) }}</li>
        @endforeach

    </ul>
@endif
