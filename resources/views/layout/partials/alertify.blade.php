@if(session()->has('flash'))
    <script>
        alertify.notify('{!! session('flash') !!}', '{{ session('flashType', 'success') }}');
    </script>
@endif