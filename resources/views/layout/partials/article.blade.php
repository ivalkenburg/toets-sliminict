<article class="panel panel-default">
    <div class="panel-heading">
        <h2 class="is-marginless"><a href="{{ route('news.show', $article->id) }}">{{ $article->asLocale('title') }}</a></h2>
    </div>
    <div class="panel-body">
        {!! $article->asLocale('body') !!}
    </div>
    <div class="panel-footer">
        <span class="text-primary">{{ $article->author->name }}</span> {{ __('wrote') }} {{ $article->created_at->diffForHumans() }}.
    </div>
</article>