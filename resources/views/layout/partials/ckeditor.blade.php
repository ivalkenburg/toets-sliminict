@push('js')
    <script src="//cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script>
    <script>
        @foreach($textareas as $textarea)
            CKEDITOR.replace('{{ $textarea }}', {
                language: '{{ config('app.locale') }}',
                height: 500,
                resize_enabled: false,
                toolbarGroups: [
                    {"name": "basicstyles", "groups": ["basicstyles"]},
                    {"name": "links", "groups": ["links"]},
                    {"name": "paragraph", "groups": ["list", "blocks"]},
                    {"name": "document", "groups": ["mode"]},
                    {"name": "insert", "groups": ["insert"]},
                    {"name": "styles", "groups": ["styles"]},
                    {"name": "about", "groups": ["about"]}
                ],
                removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
            });
        @endforeach
    </script>
@endpush