@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <script>
        $('{{ $table }}').DataTable({
            pageLength: 50,
            responsive: true,

            @if($features === false)
                info: false,
                searching: false,
                paging: false,
            @endif

            language: {!! json_encode(__('datatables')) !!},
            columnDefs: [
                {'orderable': false, 'targets': -1}
            ]
        });
    </script>
@endpush