@push('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/1.2.1/bootstrap-filestyle.min.js"></script>
    <script>
        $(":file").filestyle({
            icon: false,
            buttonText: '{{ __('Choose file') }}'
        });
    </script>
@endpush