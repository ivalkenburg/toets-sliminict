@push('js')
    <script src="{{ asset('/js/multiselect.min.js') }}"></script>
    <script>
        $('{!! $multiselect !!}').multiselect({
            search: {
                left: '<input type="text" class="form-control mb10" placeholder="{{ __('Search') }} ...">',
                right: '<input type="text" class="form-control mb10" placeholder="{{ __('Search') }} ...">',
            },
            fireSearch: function (value) {
                return value.length > 0;
            }
        });
    </script>
@endpush