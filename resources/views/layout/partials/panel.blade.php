<div{!! isset($id) ? " id=\"$id\"" : '' !!} class="panel panel-default">
    @isset($heading)
        <div class="panel-heading">
            {{ $heading }}
        </div>
    @endisset
    <div class="panel-body">
        {{ $slot }}
    </div>
    @isset($footer)
        <div class="panel-footer">
            {{ $footer }}
        </div>
    @endisset
</div>