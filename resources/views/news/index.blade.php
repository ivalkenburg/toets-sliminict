@extends('layout.public')

@section('meta')
    <title>{{ config('app.name') }} - {{ __('News') }}</title>
@endsection

@section('content')
    @foreach($articles as $article)
        @include('layout.partials.article', ['article' => $article])
    @endforeach

    <div class="text-center">
        {{  $articles->links() }}
    </div>
@endsection