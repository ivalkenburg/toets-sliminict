@extends('layout.public')

@section('meta')
    <title>{{ config('app.name') }} - {{ $article->asLocale('title') }}</title>
@endsection

@section('content')
    @include('layout.partials.article', ['article' => $article])
@endsection