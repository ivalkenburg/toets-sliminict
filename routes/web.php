<?php

Route::get('/', 'HomeController@index')->name('home');

// News Articles
Route::get('/news/', 'NewsController@index')->name('news.index.public');
Route::get('/news/{article}', 'NewsController@show')->name('news.show');

// Courses
Route::get('/courses', 'CoursesController@index')->name('courses.index.public');

//
// Admin Panel
//

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::group(['middleware' => 'guest'], function () {

        // Authentication
        Route::get('/login', 'AuthController@showLogin')->name('login');
        Route::post('/login', 'AuthController@login');

        // User Activation
        Route::get('/users/activate/{token}', 'UsersController@showActivateForm')->name('users.activateForm');
        Route::post('/users/activate', 'UsersController@activateUser')->name('users.activate');
    });

    Route::group(['middleware' => 'auth'], function () {

        // Misc
        Route::get('/logout', 'AuthController@logout')->name('logout');
        Route::get('/', 'HomeController@index')->name('admin.home');
        Route::post('/store', 'HomeController@store');

        // News
        Route::resource('news', 'NewsController', ['except' => 'show', 'parameters' => ['news' => 'article']]);

        // Users
        Route::resource('users', 'UsersController', ['only' => ['index', 'store', 'destroy']]);

        // Courses
        Route::resource('courses', 'CoursesController', ['except' => 'show']);

        // Groups
        Route::resource('groups', 'GroupsController');

        // Students
        Route::resource('students', 'StudentsController');
        Route::get('/students/{student}/email', 'StudentsController@showEmailForm')->name('students.email');
        Route::post('/students/{student}/email', 'StudentsController@emailStudent');
        Route::post('/students/{student}/comments', 'StudentsController@postComment')->name('students.comments');
        Route::post('/students/{student}/leave/{group}', 'StudentsController@leaveGroup')->name('students.leaveGroup');
    });
});
