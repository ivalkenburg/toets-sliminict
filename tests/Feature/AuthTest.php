<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    /** @test */
    public function guests_get_redirected_to_login()
    {
        $this->withExceptionHandling();

        $this->get('/admin')
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function guests_can_not_logout()
    {
        $this->withExceptionHandling();

        $this->get('/admin/logout')
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function an_authenticated_user_can_not_login()
    {
        $this->withExceptionHandling()->signIn();

        $this->get(route('login'))
             ->assertRedirect('/admin');
    }

    /** @test */
    public function a_guest_can_login_with_valid_credentials()
    {
        create('App\User', [
            'email'    => 'test@test.com',
            'password' => bcrypt('secret'),
        ]);

        $this->post('/admin/login', [
            'email'    => 'test@test.com',
            'password' => 'secret',
        ])->assertRedirect(route('admin.home'));
    }

    /** @test */
    public function a_guest_can_not_login_with_invalid_credentials()
    {
        $this->withExceptionHandling();

        $this->post('/admin/login', [
            'email'    => 'test@test.com',
            'password' => 'secret',
        ])->assertSessionHasErrors('email');
    }
}
