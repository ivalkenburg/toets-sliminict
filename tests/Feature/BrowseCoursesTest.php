<?php

namespace Tests\Feature;

use Tests\TestCase;

class BrowseCoursesTest extends TestCase
{
    /** @test */
    public function a_guest_can_browse_the_available_courses()
    {
        $coursesVisible = create('App\Course', [], 3);
        $courseInvisible = create('App\Course', ['visible' => false]);

        $response = $this->get(route('courses.index.public'));

        foreach ($coursesVisible as $course) {
            $response->assertSee($course->asLocale('name'))
                     ->assertSee($course->asLocale('description'));
        }

        $response->assertDontSee($courseInvisible->asLocale('name'));
    }
}
