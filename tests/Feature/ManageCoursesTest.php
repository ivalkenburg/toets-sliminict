<?php

namespace Tests\Feature;

use Tests\TestCase;

class ManageCoursesTest extends TestCase
{
    /** @test */
    public function an_authenticated_user_can_store_a_new_course()
    {
        $this->signIn();

        $course = make('App\Course');

        $this->post(route('courses.store'), $course->toArray());

        $this->get('/courses')
             ->assertSee($course->asLocale('name'))
             ->assertSee($course->asLocale('description'));
    }

    /** @test */
    public function guests_can_not_create_a_new_course()
    {
        $this->withExceptionHandling();

        $this->post(route('courses.store'), [])
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function an_authenticated_user_can_browse_courses()
    {
        $this->signIn();

        $courses = [];
        $courses[] = create('App\Course');
        $courses[] = create('App\Course', ['visible' => false]);

        $response = $this->get(route('courses.index'));

        foreach ($courses as $course) {
            $response->assertSee($course->asLocale('name'));
        }
    }

    /** @test */
    public function an_authenticated_user_can_edit_course()
    {
        $this->withExceptionHandling()->signIn();

        $course = create('App\Course');
        $modifiedCourse = make('App\Course');

        $this->patch(route('courses.update', $course->id), $modifiedCourse->toArray())
             ->assertRedirect(route('courses.index'));

        $this->get(route('courses.index.public'))
             ->assertSee($modifiedCourse->asLocale('name'))
             ->assertSee($modifiedCourse->asLocale('description'))
             ->assertSee($modifiedCourse->price());
    }

    /** @test */
    public function an_authenticated_user_can_delete_a_course()
    {
        $this->withExceptionHandling()->signIn();

        $course = create('App\Course');

        $this->delete(route('courses.destroy', $course->id))
             ->assertRedirect(route('courses.index'));

        $this->get(route('courses.index'))
             ->assertDontSee($course->asLocale('name'));
    }
}
