<?php

namespace Tests\Feature;

use Tests\TestCase;

class ManageGroupTest extends TestCase
{
    /** @test */
    public function an_authenticated_user_can_create_a_group()
    {
        $this->withExceptionHandling()->signIn();

        $course = create('App\Course');
        $students = create('App\Student', [], 5);

        $group = make('App\Group', [
            'course_id' => $course->id,
            'students'  => $students->pluck('id')->toArray(),
        ]);

        $response = $this->post(route('groups.store', $group->toArray()));

        $response = $this->get($response->headers->get('Location'));
        $response->assertSee($group->name)
                 ->assertSee($group->comment)
                 ->assertSee($course->asLocale('name'));

        foreach ($students as $student) {
            $response->assertSee($student->name);
        }
    }
}
