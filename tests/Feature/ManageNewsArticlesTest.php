<?php

namespace Tests\Feature;

use App\NewsArticle;
use Tests\TestCase;

class ManageNewsArticlesTest extends TestCase
{
    /** @test */
    public function an_authenticated_user_can_browse_news_articles()
    {
        $this->signIn();

        $articles = create('App\NewsArticle', [], 2);

        $response = $this->get(route('news.index'));

        foreach ($articles as $article) {
            $response->assertSee($article->asLocale('title'));
        }
    }

    /** @test */
    public function an_admin_can_publish_a_news_article()
    {
        $this->signIn();

        $article = make('App\NewsArticle');

        $response = $this->post(route('news.store'), $article->toArray());

        $this->get($response->headers->get('Location'))
             ->assertSee($article->asLocale('title'))
             ->assertSee($article->asLocale('body'));
    }

    /** @test */
    public function an_admin_can_edit_a_news_article()
    {
        $this->signIn();

        $article = create('App\NewsArticle');

        $modifiedArticle = $article->toArray();

        $modifiedArticle['title'][$this->default] = $this->faker->sentence;
        $modifiedArticle['body'][$this->default] = $this->faker->paragraph;

        $this->patch(route('news.update', $article->id), $modifiedArticle);

        $this->get(route('news.show', $article->id))
             ->assertSee($modifiedArticle['title'][$this->default])
             ->assertSee($modifiedArticle['body'][$this->default]);
    }

    /** @test */
    public function an_admin_can_delete_a_news_article()
    {
        $this->withExceptionHandling()->signIn();

        $article = create('App\NewsArticle');

        $this->delete(route('news.destroy', $article->id));

        $this->get(route('news.show', $article->id))
             ->assertStatus(404);

        $this->assertEquals(0, NewsArticle::count());
    }

    /** @test */
    public function a_guest_can_not_edit_a_new_article()
    {
        $this->withExceptionHandling();

        $article = create('App\NewsArticle');

        $this->get(route('news.edit', $article->id))
             ->assertRedirect(route('login'));

        $this->patch(route('news.update', $article->id), [])
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function a_guest_can_not_publish_a_news_article()
    {
        $this->withExceptionHandling();

        $this->get(route('news.create'))
             ->assertRedirect(route('login'));

        $this->post(route('news.store'), [])
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function a_news_article_requires_fallback_locale_title()
    {
        $this->withExceptionHandling()->signIn();

        $article = make('App\NewsArticle', ['title' => [$this->fallback => '']]);

        $this->post(route('news.store'), $article->toArray())
             ->assertSessionHasErrors('title.'.$this->fallback);
    }

    /** @test */
    public function a_news_article_requires_fallback_locale_body()
    {
        $this->withExceptionHandling()->signIn();

        $article = make('App\NewsArticle', ['body' => [$this->fallback => '']]);

        $this->post(route('news.store'), $article->toArray())
             ->assertSessionHasErrors('body.'.$this->fallback);
    }
}
