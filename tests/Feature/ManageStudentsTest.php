<?php

namespace Tests\Feature;

use App\Mail\StudentMail;
use App\Student;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ManageStudentsTest extends TestCase
{
    /** @test */
    public function an_authenticated_user_can_send_an_email_to_students()
    {
        $this->withExceptionHandling()->signIn($user = create('App\User'));

        $student = create('App\Student');

        $mail = [
            'subject' => $this->faker->sentence,
            'body'    => $this->faker->paragraphs(5, true),
        ];

        $this->post(route('students.email', $student->id), $mail)
             ->assertRedirect(route('students.show', $student->id));

        Mail::assertSent(StudentMail::class, function ($mail) use ($student) {
            return $mail->hasTo($student);
        });
    }

    /** @test */
    public function a_guest_can_not_send_an_email_to_students()
    {
        $this->withExceptionHandling();

        $this->post(route('students.email', 1), [])
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function an_authenticated_user_can_post_a_comment_about_a_student()
    {
        $this->signIn();

        $student = create('App\Student');

        $comment = make('App\StudentComment');

        $this->post(route('students.comments', $student->id), $comment->toArray())
             ->assertRedirect(route('students.show', $student->id));

        $this->get(route('students.show', $student->id))
             ->assertSee($comment->body);
    }

    /** @test */
    public function a_guest_can_not_post_a_comment_to_a_student()
    {
        $this->withExceptionHandling();

        $this->post(route('students.comments', 1), [])
             ->assertRedirect(route('login'));
    }

    /** @test */
    public function a_comment_requires_a_body()
    {
        $this->withExceptionHandling()->signIn();

        $student = create('App\Student');

        $comment = make('App\StudentComment', ['body' => null]);

        $this->post(route('students.comments', $student->id), $comment->toArray())
             ->assertSessionHasErrors('body');
    }

    /** @test */
    public function an_authenticated_user_can_add_a_new_student()
    {
        $this->withExceptionHandling()->signIn();

        $student = make('App\Student', ['picture' => UploadedFile::fake()->image('picture.png', 400, 400)]);

        $response = $this->post(route('students.store'), $student->toArray());

        $this->get($response->headers->get('Location'))
             ->assertSee($student->name)
             ->assertDontSee(config('app.students_default_img'));
    }

    /** @test */
    public function an_authenticated_user_can_add_edit_a_student()
    {
        $this->withExceptionHandling()->signIn();

        $student = make('App\Student', ['picture' => UploadedFile::fake()->image('picture.png', 400, 400)]);

        $this->post(route('students.store'), $student->toArray());

        $student = Student::first();

        $modifiedStudent = make('App\Student', ['no_picture' => true]);

        $this->patch(route('students.update', $student->id), $modifiedStudent->toArray())
             ->assertRedirect(route('students.show', $student->id));

        $this->get(route('students.show', $student->id))
             ->assertSee($modifiedStudent->name)
             ->assertDontSee($student->picture);
    }
}
