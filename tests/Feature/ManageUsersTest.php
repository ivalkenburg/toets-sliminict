<?php

namespace Tests\Feature;

use App\Mail\UserActivation;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ManageUsersTest extends TestCase
{
    /** @test */
    public function an_authenticated_user_can_delete_a_user()
    {
        $this->signIn();

        $user = create('App\User');

        $this->deleteJson(route('users.destroy', $user->id))
             ->assertJson(['status' => 'success']);
    }

    /** @test */
    public function a_guest_can_not_delete_a_user()
    {
        $this->withExceptionHandling();

        $this->deleteJson(route('users.destroy', 1))
             ->assertStatus(401);
    }

    /** @test */
    public function an_authenticated_user_can_add_a_user()
    {
        $this->signIn();

        $this->json('post', route('users.store'), [
            'name'  => 'John Doe',
            'email' => 'example@example.com',
        ])->assertJson(['status' => 'success']);

        Mail::assertSent(UserActivation::class, function ($mail) {
            return $mail->user->name === 'John Doe';
        });

        Mail::assertSent(UserActivation::class, function ($mail) {
            return $mail->hasTo('example@example.com');
        });

        Mail::assertSent(UserActivation::class, function ($mail) {
            return is_string($mail->user->activation_token);
        });
    }

    /** @test */
    public function a_guest_can_not_add_a_user()
    {
        $this->withExceptionHandling();

        $this->json('post', route('users.store', []))
             ->assertStatus(401);
    }

    /** @test */
    public function a_guest_can_activate_their_account()
    {
        $user = create('App\User', [
            'password'         => null,
            'activation_token' => md5(uniqid()),
        ]);

        $response = $this->post(route('users.activate', $user->activation_token), [
            'email'                 => $user->email,
            'password'              => 'password12345',
            'password_confirmation' => 'password12345',
        ]);

        $this->get($response->headers->get('Location'))
             ->assertSee($user->name);

        $this->assertDatabaseHas('users', [
            'id'               => $user->id,
            'email'            => $user->email,
            'activation_token' => null,
        ]);
    }
}
