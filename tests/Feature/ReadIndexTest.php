<?php

namespace Tests\Feature;

use App\Meta;
use App\NewsArticle;
use Tests\TestCase;

class ReadIndexTest extends TestCase
{
    /** @test */
    public function guests_can_read_index_in_default_language()
    {
        $index_body = [];

        foreach ($this->locales as $locale) {
            $index_body[$locale] = $this->faker->paragraph;
        }

        Meta::set('index_body', $index_body);

        $this->get('/')
             ->assertSee($index_body[$this->default]);
    }

    /** @test */
    public function guests_can_set_language_and_read_set_language()
    {
        $index_body = [];

        foreach ($this->locales as $locale) {
            $index_body[$locale] = $this->faker->paragraph;
        }

        Meta::set('index_body', $index_body);

        $this->get("/?lang={$this->fallback}")
             ->assertSee($index_body[$this->fallback]);

        $this->get('/')
             ->assertSee($index_body[$this->fallback]);
    }

    /** @test */
    public function guests_can_see_the_last_news_post()
    {
        create('App\NewsArticle', [], 5);

        $last = NewsArticle::last();

        $this->get('/')
             ->assertSee($last->asLocale('title'))
             ->assertSee($last->asLocale('body'));
    }

    /** @test */
    public function an_authenticated_user_can_set_the_index_body()
    {
        $this->signIn();

        $index_body = [];

        foreach ($this->locales as $locale) {
            $index_body[$locale] = $this->faker->paragraph;
        }

        $this->post('/admin/store', [
            'body' => $index_body,
        ]);

        $this->get('/')
             ->assertSee($index_body[config('app.locale')]);
    }

    /** @test */
    public function guest_can_not_set_the_index_body()
    {
        $this->withExceptionHandling();

        $this->post('/admin/store', [])
             ->assertRedirect(route('login'));
    }
}
