<?php

namespace Tests\Feature;

use App\NewsArticle;
use Tests\TestCase;

class ReadNewsArticleTest extends TestCase
{
    /** @test */
    public function guests_can_see_paginated_news_items()
    {
        create('App\NewsArticle', [], 12);

        $this->get('/news')
             ->assertSee('news?page=2')
             ->assertSee('news?page=3');
    }

    /** @test */
    public function guests_can_see_a_single_news_item()
    {
        create('App\NewsArticle', [], 12);

        $last = NewsArticle::last();

        $this->get(route('news.show', $last->id))
             ->assertSee($last->asLocale('title'))
             ->assertSee($last->asLocale('body'))
             ->assertSee(e($last->author->name));
    }
}
