<?php

namespace Tests;

use App\Exceptions\Handler;
use Carbon\Carbon;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected $oldExceptionHandler;

    /** @var Generator */
    protected $faker;

    protected $default;
    protected $fallback;
    protected $locales;

    protected function setUp()
    {
        parent::setUp();

        // Reset carbon to current time
        Carbon::setTestNow();

        // Prevent mail from being send
        Mail::fake();

        // Disable automatic exception handling
        $this->disableExceptionHandling();

        $this->locales = config('app.locales');
        $this->fallback = config('app.fallback_locale');
        $this->default = config('app.locale');
        $this->faker = Factory::create();

        // Enable foreign keys for sqlite
        DB::statement(DB::raw('PRAGMA foreign_keys = ON;'));
    }

    protected function signIn($user = null)
    {
        $user = $user ?: factory('App\User')->create();

        $this->be($user);

        return $this;
    }

    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);

        $this->app->instance(ExceptionHandler::class, new class() extends Handler {
            public function __construct()
            {
            }

            public function report(\Exception $e)
            {
            }

            public function render($request, \Exception $e)
            {
                throw $e;
            }
        });
    }

    protected function withExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, $this->oldExceptionHandler);

        return $this;
    }
}
