<?php

namespace Tests\Unit;

use App\Course;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class CourseTest extends TestCase
{
    /** @test */
    public function it_has_many_groups()
    {
        $course = create('App\Course');

        create('App\Group', ['course_id' => $course->id], 5);

        $this->assertInstanceOf(Collection::class, $course->groups);
        $this->assertCount(5, $course->groups);
    }

    /** @test */
    public function it_gives_only_visible_courses()
    {
        create('App\Course');
        create('App\Course', ['visible' => false]);

        $this->assertCount(1, Course::visible()->get());
    }
}
