<?php

namespace Tests\Unit;

use App\Course;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class GroupTest extends TestCase
{
    /** @test */
    public function it_belongs_to_a_course()
    {
        $group = create('App\Group');

        $this->assertInstanceOf(Course::class, $group->course);
    }

    /** @test */
    public function it_has_many_students()
    {
        $group = create('App\Group');

        create('App\Student', [], 10)->each(function ($student) use ($group) {
            $group->students()->attach($student);
        });

        $this->assertInstanceOf(Collection::class, $group->students);
        $this->assertCount(10, $group->students);
    }

    /** @test */
    public function it_can_add_a_student()
    {
        $group = create('App\Group');
        $student = create('App\Student');

        $group->addStudent($student);

        $this->assertCount(1, $group->students);
    }

    /** @test */
    public function it_can_remove_a_student()
    {
        $group = create('App\Group');
        $student = create('App\Student');

        $group->students()->attach($student);

        $this->assertCount(1, $group->students);

        $group->removeStudent($student);

        $group->refresh();

        $this->assertCount(0, $group->students);
    }

    /** @test */
    public function it_can_sync_students()
    {
        $group = create('App\Group');
        $students = create('App\Student', [], 12);

        $group->syncStudents($students);

        $this->assertEquals(12, $group->students()->count());

        $students->push(create('App\Student'));

        $group->syncStudents($students);

        $this->assertEquals(13, $group->students()->count());
    }
}
