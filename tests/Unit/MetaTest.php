<?php

namespace Tests\Unit;

use App\Meta;
use Tests\TestCase;

class MetaTest extends TestCase
{
    /** @test */
    public function it_automatically_serializes_unserializes_value()
    {
        $value = [
            'foo' => 'bar',
            'baz' => 'qiz',
        ];

        $meta = Meta::set('foo', $value);

        $this->assertDatabaseHas('metas', ['value' => serialize($value)]);
        $this->assertEquals($value, $meta->value);
    }

    /** @test */
    public function it_can_get_value_as_locale()
    {
        $value = [$this->fallback => 'fallback'];

        $meta = Meta::set('foo', $value);

        $this->assertEquals($value[$this->fallback], $meta->asLocale());
    }
}
