<?php

namespace Tests\Unit;

use App\NewsArticle;
use App\User;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class NewsArticleTest extends TestCase
{
    /** @test */
    public function it_belongs_to_an_author()
    {
        $user = create('App\User');
        $news = create('App\NewsArticle', ['user_id' => $user->id]);

        $this->assertInstanceOf(User::class, $news->author);
    }

    /** @test */
    public function it_serializes_title()
    {
        $title = [
            'nl' => 'Dit is in het nederlands.',
            'en' => 'This is in english.',
        ];

        create('App\NewsArticle', ['title' => $title]);

        $news = NewsArticle::find(1);

        $this->assertEquals($title, $news->title);
    }

    /** @test */
    public function it_serializes_body()
    {
        $body = [
            'nl' => 'Dit is in het nederlands.',
            'en' => 'This is in english.',
        ];

        create('App\NewsArticle', ['body' => $body]);

        $news = NewsArticle::find(1);

        $this->assertEquals($body, $news->body);
    }

    public function it_can_get_string_as_locale()
    {
        $title = [];
        $body = [];

        foreach ($this->locales as $locale) {
            $title[$locale] = $this->faker->sentence;
            $body[$locale] = $this->faker->paragraph;
        }

        $source = ['title' => $title, 'body' => $body];

        foreach ($this->locales as $locale) {
            App::setLocale($locale);

            $article = create('App\NewsArticle', $source);

            $this->assertEquals($source['title'][$locale], $article->asLocale('title'));
            $this->assertEquals($source['body'][$locale], $article->asLocale('body'));
        }
    }

    /** @test */
    public function it_deletes_when_author_is_deleted()
    {
        $article = create('App\NewsArticle');

        $this->assertEquals(1, NewsArticle::count());

        $article->author->delete();

        $this->assertEquals(0, NewsArticle::count());
    }
}
