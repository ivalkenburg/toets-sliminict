<?php

namespace Tests\Unit;

use App\Student;
use App\StudentComment;
use App\User;
use Tests\TestCase;

class StudentCommentTest extends TestCase
{
    /** @test */
    public function it_belongs_to_a_student()
    {
        $comment = create('App\StudentComment');

        $this->assertInstanceOf(Student::class, $comment->student);
    }

    /** @test */
    public function it_belongs_to_an_author()
    {
        $comment = create('App\StudentComment');

        $this->assertInstanceOf(User::class, $comment->author);
    }

    /** @test */
    public function it_deletes_when_student_is_deleted()
    {
        $comment = create('App\StudentComment');

        $this->assertEquals(1, StudentComment::count());

        $comment->student->delete();

        $this->assertEquals(0, StudentComment::count());
    }

    /** @test */
    public function it_deletes_when_author_is_deleted()
    {
        $comment = create('App\StudentComment');

        $this->assertEquals(1, StudentComment::count());

        $comment->author->delete();

        $this->assertEquals(0, StudentComment::count());
    }
}
