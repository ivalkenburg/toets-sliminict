<?php

namespace Tests\Unit;

use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class StudentTest extends TestCase
{
    /** @test */
    public function it_has_many_comments()
    {
        $student = create('App\Student');

        create('App\StudentComment', ['student_id' => $student->id], 5);

        $this->assertInstanceOf(Collection::class, $student->comments);
        $this->assertCount(5, $student->comments);
    }

    /** @test */
    public function it_has_many_groups()
    {
        $student = create('App\Student');

        create('App\Group', [], 10)->each(function ($group) use ($student) {
            $student->groups()->attach($group);
        });

        $this->assertInstanceOf(Collection::class, $student->groups);
        $this->assertCount(10, $student->groups);
    }

    /** @test */
    public function it_can_join_a_group()
    {
        $student = create('App\Student');
        $group = create('App\Group');

        $student->joinGroup($group);

        $this->assertCount(1, $student->groups);
    }

    /** @test */
    public function it_can_leave_a_group()
    {
        $student = create('App\Student');
        $group = create('App\Group');

        $student->groups()->attach($group);

        $this->assertCount(1, $student->groups);

        $student->leaveGroup($group);

        $student->refresh();

        $this->assertCount(0, $student->groups);
    }

    /** @test */
    public function it_can_sync_groups()
    {
        $student = create('App\Student');

        $groups = create('App\Group', [], 5);

        $student->syncGroups($groups);

        $this->assertEquals(5, $student->groups()->count());

        $groups->push(create('App\Group'));

        $student->syncGroups($groups);

        $this->assertEquals(6, $student->groups()->count());
    }
}
