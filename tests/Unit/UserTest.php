<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function it_has_many_articles()
    {
        $user = create('App\User');

        create('App\NewsArticle', ['user_id' => $user->id], 2);

        $this->assertCount(2, $user->articles);
    }

    /** @test */
    public function it_has_many_student_comments()
    {
        $user = create('App\User');

        create('App\StudentComment', ['author_id' => $user->id], 10);

        $this->assertInstanceOf(Collection::class, $user->studentComments);
        $this->assertCount(10, $user->studentComments);
    }

    /** @test */
    public function it_can_add_a_user()
    {
        $user = [
            'name'  => 'John Doe',
            'email' => 'example@example.com',
        ];

        $user = User::add($user);

        $this->assertInstanceOf(User::class, $user);

        $this->assertTrue(is_string($user->activation_token));
    }
}
