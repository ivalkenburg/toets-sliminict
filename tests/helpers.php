<?php

function make($model, $overrides = [], $amount = null)
{
    return factory($model, $amount)->make($overrides);
}

function create($model, $overrides = [], $amount = null)
{
    return factory($model, $amount)->create($overrides);
}
