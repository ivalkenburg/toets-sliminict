# TODO

### News

- Hide/unhide news article by ajax...
- Comments? To much time maybe..?
- Slugs.
- Ajax drivin [DataTables](https://datatables.yajrabox.com/).
- vue.js powered?

### Localization

- Proper URL handling instead of query variable. [https://github.com/mcamara/laravel-localization](https://github.com/mcamara/laravel-localization)
- Datatables localization

### Visual

- Bootstrap 4, fucking menu tho in mobile.
- datatables dom

### Responsiveness

- https://datatables.net/extensions/responsive/examples/display-types/bootstrap-modal.html